module.exports = {
  mode: 'jit',
  purge: {
    content: [
      'components/**/*.{vue,js}',
      'layouts/**/*.vue',
      'pages/**/*.vue',
      'plugins/**/*.{js,ts}',
      'nuxt.config.{js,ts}'
    ]
  },
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      fontFamily: {
        lato: ['Lato']
      },
      colors: {
        aqua: '#319795',
        bgaqua: '#81E6D9',
        lightaqua: '#E6FFFA',
        gray: {
          title: '#2D3748',
          steps: '#718096'
        }
      }
    }
  },
  variants: {
    extend: {}
  },
  plugins: []
}
